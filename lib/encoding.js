'use strict';

// The purpose of this module is to abstract away
// the parts that aren't vanilla js (for easy portability)
// and to work with native JavaScript Uint8Arrays

var Enc = module.exports;

Enc.base64ToBuf = function (str) {
  return Buffer.from(str, 'base64');
};

Enc.bufToBase64 = function (u8) {
  return Buffer.from(u8).toString('base64');
};

Enc.bufToBin = function (u8) {
  return Buffer.from(u8).toString('binary');
};

Enc.bufToHex = function (u8) {
  return Buffer.from(u8).toString('hex');
};

Enc.bufToUrlBase64 = function (u8) {
  return Enc.bufToBase64(u8)
    .replace(/\+/g, '-').replace(/\//g, '_').replace(/=/g, '');
};
