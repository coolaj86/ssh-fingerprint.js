# SSH to JWK (for node.js)

A minimal library to parse an SSH public key (`id_rsa.pub`)
and convert it into a public JWK.

Works for RSA and ECDSA public keys.

Features
========

&lt; 100 lines of code | <1kb gzipped | 1.8kb minified | 3.1kb with comments

* [x] SSH Public Keys
  * fingerprint
* [x] RSA Public Keys
* [x] EC Public Keys
  * P-256 (prime256v1, secp256r1)
  * P-384 (secp384r1)
* [x] Browser Version
  * [Bluecrypt SSH to JWK](https://git.coolaj86.com/coolaj86/bluecrypt-ssh-to-jwk.js)

### Need JWK to SSH? SSH to PEM?

Try one of these:

* [jwk-to-ssh.js](https://git.coolaj86.com/coolaj86/jwk-to-ssh.js) (RSA + EC)
* [Eckles.js](https://git.coolaj86.com/coolaj86/eckles.js) (more EC utils)
* [Rasha.js](https://git.coolaj86.com/coolaj86/eckles.js) (more RSA utils)

### Need SSH Private Keys?

SSH private keys are just normal PEM files,
so you can use Eckles or Rasha, as mentioned above.

# CLI

You can install `ssh-to-jwk` and use it from command line:

```bash
npm install -g ssh-to-jwk
```

```bash
ssh-to-jwk ~/.ssh/id_rsa.pub
```

# Usage

You can also use it from JavaScript:

**SSH to JWK**

```js
var fs = require('fs');
var sshtojwk = require('ssh-to-jwk');

var pub = fs.readFileSync("./id_rsa.pub");
var ssh = sshtojwk.parse({ pub: pub });

console.info(ssh.jwk);
```

**SSH Fingerprint**

```js
var fs = require('fs');
var sshtojwk = require('ssh-to-jwk');
var pub = fs.readFileSync("./id_rsa.pub");

sshtojwk.fingerprint({ pub: pub }).then(function (fingerprint) {
  console.info(fingerprint);
  // SHA256:yCB62vBVsOwqksgYwy/WDbaMF2PhPijAwcrlzmrxfko
});
```

# Legal

[ssh-to-jwk.js](https://git.coolaj86.com/coolaj86/ssh-to-jwk.js) |
MPL-2.0 |
[Terms of Use](https://therootcompany.com/legal/#terms) |
[Privacy Policy](https://therootcompany.com/legal/#privacy)
