#!/usr/bin/env node
'use strict';

var fs = require('fs');
var path = require('path');
var sshtojwk = require('../index.js');

var pubfile = process.argv[2];

if (!pubfile) {
  pubfile = path.join(require('os').homedir(), '.ssh/id_rsa.pub');
}

var buf = fs.readFileSync(pubfile);
var pub = buf.toString('ascii');
var ssh = sshtojwk.parse({ pub: pub });

// Finally! https://superuser.com/a/714195
sshtojwk.fingerprint({ pub: pub }).then(function (fingerprint) {
  console.warn('The key fingerprint is:\n' + fingerprint + ' ' + ssh.comment);
  console.info(JSON.stringify(ssh.jwk, null, 2));
});
